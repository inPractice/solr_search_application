﻿using System.Web.Mvc;
using SolrNet;
using SolrNet.Commands.Parameters;
using Microsoft.Practices.ServiceLocation;
using ClinicalCareOptions.inPractice.Search.Models;
using System.Linq;

namespace ClinicalCareOptions.inPractice.Search.Controllers
{
	public class HomeController : Controller
	{
        ISolrOperations<Hepatology> hepatologySolr = ServiceLocator.Current.GetInstance<ISolrOperations<Hepatology>>();
        ISolrOperations<Rheumatology> rheumatologySolr = ServiceLocator.Current.GetInstance<ISolrOperations<Rheumatology>>();
        ISolrOperations<Guidelines> guidelinesSolr = ServiceLocator.Current.GetInstance<ISolrOperations<Guidelines>>();
        ISolrOperations<Drugs> drugsSolr = ServiceLocator.Current.GetInstance<ISolrOperations<Drugs>>();
        ISolrOperations<Oncology> oncologySolr = ServiceLocator.Current.GetInstance<ISolrOperations<Oncology>>();
        ISolrOperations<OncologyNursing> oncologyNursingSolr = ServiceLocator.Current.GetInstance<ISolrOperations<OncologyNursing>>();
        ISolrOperations<HIV> hivSolr = ServiceLocator.Current.GetInstance<ISolrOperations<HIV>>();


        public ISolrQuery InPracticeSolrQuery(Parameters Params)
		{
			if (!string.IsNullOrEmpty(Params.Search))
			{
				return new SolrQuery(Params.Search);
			}
			return SolrQuery.All;
		}

		public ActionResult Index(Parameters Params)
		{
			var start = (Params.PageNum - 1) * Params.MaxResultsPage;

            switch (Params.Collection)
            {
                default:
                    var hResults = hepatologySolr.Query(InPracticeSolrQuery(Params), new QueryOptions
                    {
                        Rows = Params.MaxResultsPage,
                        Start = start,
                        SpellCheck = new SpellCheckingParameters(),
                    });
                    var hView = new HepatologyResultsView
                    {
                        Documents = hResults,
                        Params = Params,
                        Count = hResults.NumFound,
                        DidYouMean = string.Join(" ", hResults.SpellChecking
                                        .Select(c => c.Suggestions.FirstOrDefault())
                                        .Where(c => !string.IsNullOrEmpty(c))
                                        .ToArray()),
                    };

                    return View(hView);
                case Collections.Rheumatology:
                    var rResults = rheumatologySolr.Query(InPracticeSolrQuery(Params), new QueryOptions
                    {
                        Rows = Params.MaxResultsPage,
                        Start = start,
                        SpellCheck = new SpellCheckingParameters(),
                    });
                    var rView = new RheumatologyResultsView
                    {
                        Documents = rResults,
                        Params = Params,
                        Count = rResults.NumFound,
                        DidYouMean = string.Join(" ", rResults.SpellChecking
                                        .Select(c => c.Suggestions.FirstOrDefault())
                                        .Where(c => !string.IsNullOrEmpty(c))
                                        .ToArray()),
                    };

                    return View(rView);
                case Collections.Guidelines:
                    var gResults = guidelinesSolr.Query(InPracticeSolrQuery(Params), new QueryOptions
                    {
                        Rows = Params.MaxResultsPage,
                        Start = start,
                        SpellCheck = new SpellCheckingParameters(),
                    });
                    var gView = new GuidelinesResultsView
                    {
                        Documents = gResults,
                        Params = Params,
                        Count = gResults.NumFound,
                        DidYouMean = string.Join(" ", gResults.SpellChecking
                                        .Select(c => c.Suggestions.FirstOrDefault())
                                        .Where(c => !string.IsNullOrEmpty(c))
                                        .ToArray()),
                    };

                    return View(gView);
                case Collections.Drugs:
                    var dResults = drugsSolr.Query(InPracticeSolrQuery(Params), new QueryOptions
                    {
                        Rows = Params.MaxResultsPage,
                        Start = start,
                        SpellCheck = new SpellCheckingParameters(),
                    });
                    var dView = new DrugsResultsView
                    {
                        Documents = dResults,
                        Params = Params,
                        Count = dResults.NumFound,
                        DidYouMean = string.Join(" ", dResults.SpellChecking
                                        .Select(c => c.Suggestions.FirstOrDefault())
                                        .Where(c => !string.IsNullOrEmpty(c))
                                        .ToArray()),
                    };

                    return View(dView);
                case Collections.Oncology:
                    var oResults = oncologySolr.Query(InPracticeSolrQuery(Params), new QueryOptions
                    {
                        Rows = Params.MaxResultsPage,
                        Start = start,
                        SpellCheck = new SpellCheckingParameters(),
                    });
                    var oView = new OncologyResultsView
                    {
                        Documents = oResults,
                        Params = Params,
                        Count = oResults.NumFound,
                        DidYouMean = string.Join(" ", oResults.SpellChecking
                                        .Select(c => c.Suggestions.FirstOrDefault())
                                        .Where(c => !string.IsNullOrEmpty(c))
                                        .ToArray()),
                    };

                    return View(oView);
                case Collections.OncologyNursing:
                    var oNResults = oncologyNursingSolr.Query(InPracticeSolrQuery(Params), new QueryOptions
                    {
                        Rows = Params.MaxResultsPage,
                        Start = start,
                        SpellCheck = new SpellCheckingParameters(),
                    });
                    var oNView = new OncologyNursingResultsView
                    {
                        Documents = oNResults,
                        Params = Params,
                        Count = oNResults.NumFound,
                        DidYouMean = string.Join(" ", oNResults.SpellChecking
                                        .Select(c => c.Suggestions.FirstOrDefault())
                                        .Where(c => !string.IsNullOrEmpty(c))
                                        .ToArray()),
                    };

                    return View(oNView);
                case Collections.HIV:
                    var hivResults = hivSolr.Query(InPracticeSolrQuery(Params), new QueryOptions
                    {
                        Rows = Params.MaxResultsPage,
                        Start = start,
                        SpellCheck = new SpellCheckingParameters(),
                    });
                    var hivView = new HIVResultsView
                    {
                        Documents = hivResults,
                        Params = Params,
                        Count = hivResults.NumFound,
                        DidYouMean = string.Join(" ", hivResults.SpellChecking
                                        .Select(c => c.Suggestions.FirstOrDefault())
                                        .Where(c => !string.IsNullOrEmpty(c))
                                        .ToArray()),
                    };

                    return View(hivView);
            }

        }
	}
}
