#!/bin/bash
ZKCLI_PATH=/prod/solr/server/scripts/cloud-scripts/zkcli.sh
ZK1=ec2-34-224-33-121.compute-1.amazonaws.com:2181
ZK2=ec2-54-152-165-250.compute-1.amazonaws.com:2181
ZK3=ec2-54-208-223-248.compute-1.amazonaws.com:2181
CONF_PATH=/prod/configs/$1/conf/

$ZKCLI_PATH -zkhost $ZK1,$ZK2,$ZK3 -cmd upconfig -confdir $CONF_PATH -confname $1
