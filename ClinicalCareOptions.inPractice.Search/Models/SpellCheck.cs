﻿namespace ClinicalCareOptions.inPractice.Search
{
	public class SpellCheck
	{
		public string DidYouMean { get; set; }
		public string Query { get; set; }
	}
}
