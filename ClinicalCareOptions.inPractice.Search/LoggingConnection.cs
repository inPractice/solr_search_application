﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using log4net;
using SolrNet;
using System.Diagnostics;

namespace ClinicalCareOptions.inPractice.Search
{
	public class LoggingConnection : ISolrConnection
	{
		private readonly ISolrConnection connection;

		public LoggingConnection(ISolrConnection connection)
		{
			this.connection = connection;
		}

		public string Post(string relativeUrl, string s)
		{
			logger.DebugFormat("POSTing '{0}' to '{1}'", s, relativeUrl);
			return connection.Post(relativeUrl, s);
		}

		public string PostStream(string relativeUrl, string contentType, Stream content, IEnumerable<KeyValuePair<string, string>> getParameters)
		{
			logger.DebugFormat("POSTing to '{0}'", relativeUrl);
			return connection.PostStream(relativeUrl, contentType, content, getParameters);
		}

		public string Get(string relativeUrl, IEnumerable<KeyValuePair<string, string>> parameters)
		{
			var stringParams = string.Join(", ", parameters.Select(p => string.Format("{0}={1}", p.Key, p.Value)).ToArray());
            string message = string.Format("GETting '{0}' from '{1}'", stringParams, relativeUrl);

            logger.DebugFormat(message);
            Debug.WriteLine(message);

            return connection.Get(relativeUrl, parameters);
		}

		private static readonly ILog logger = LogManager.GetLogger(typeof(LoggingConnection));
	}
}