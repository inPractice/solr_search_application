﻿using ClinicalCareOptions.inPractice.Search.Models;
using System;
namespace ClinicalCareOptions.inPractice.Search
{
public class Parameters
	{
		public const int MaxResults = 10;
        public const Collections DefaultCollection = Collections.Hepatology;

		public Parameters()
		{
            MaxResultsPage = MaxResults;
			PageNum = 1;
            Collection = DefaultCollection;
		}

		public string Search { get; set; }
		public int PageNum { get; set; }
		public int MaxResultsPage { get; set; }
        public Collections Collection { get; set; }
    }
}
