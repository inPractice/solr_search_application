﻿

using System.Web.Optimization;

namespace ClinicalCareOptions.inPractice.Search.App_Start
{
    public class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/search.css"));
        }
    }
}