﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ClinicalCareOptions.inPractice.Search.Helpers
{
    public static class UrlHelperExtensions
	{
		public static string SetParameter(this UrlHelper helper, string url, string key, string value)
		{
			return helper.SetParameters(url, new Dictionary<string, object> {
				{key, value}
			});
		}

		public static string SetParameters(this UrlHelper helper, string url, IDictionary<string, object> parameters)
		{
			var parts = url.Split('?');
			IDictionary<string, string> qs = new Dictionary<string, string>();
			if (parts.Length > 1)
				qs = ParseQueryString(parts[1]);
			foreach (var p in parameters)
				qs[p.Key] = p.Value == null ? null : p.Value.ToString();
			return parts[0] + "?" + DictionaryToQueryString(qs);
		}

		public static string RemoveParametersUrl(this UrlHelper helper, string url, params string[] parameters)
		{
			var parts = url.Split('?');
			IDictionary<string, string> qs = new Dictionary<string, string>();
			if (parts.Length > 1)
				qs = ParseQueryString(parts[1]);
			foreach (var p in parameters)
				qs.Remove(p);
			return parts[0] + "?" + DictionaryToQueryString(qs);
		}

		public static string RemoveParameters(this UrlHelper helper, params string[] parameters)
		{
			return helper.RemoveParametersUrl(helper.RequestContext.HttpContext.Request.RawUrl, parameters);
		}

		public static string DictionaryToQueryString(IDictionary<string, string> qs)
		{
			return string.Join("&", qs
				.Where(k => !string.IsNullOrEmpty(k.Key))
				.Select(k => string.Format("{0}={1}", HttpUtility.UrlEncode(k.Key), HttpUtility.UrlEncode(k.Value))).ToArray());
		}

		public static string SetParameter(this UrlHelper helper, string key, object value)
		{
			return helper.SetParameter(helper.RequestContext.HttpContext.Request.RawUrl, key, value == null ? null : value.ToString());
		}

		public static string SetParameters(this UrlHelper helper, object parameterDictionary)
		{
			IDictionary<string, object> dictionary = null;

			if (parameterDictionary != null)
			{
                dictionary = parameterDictionary.GetType().GetProperties()
                                                .Select(p => new KeyValuePair<string, object>(p.Name, p.GetValue(parameterDictionary, null))).ToDictionary();
			}
			return helper.SetParameters(helper.RequestContext.HttpContext.Request.RawUrl, dictionary);
		}

		public static IDictionary<string, string> ParseQueryString(string s)
		{
			var d = new Dictionary<string, string>(StringComparer.InvariantCultureIgnoreCase);
			if (s == null)
				return d;
			if (s.StartsWith("?", StringComparison.Ordinal))
				s = s.Substring(1);
			foreach (var kv in s.Split('&'))
			{
				var v = kv.Split('=');
				if (string.IsNullOrEmpty(v[0]))
					continue;
				d[HttpUtility.UrlDecode(v[0])] = HttpUtility.UrlDecode(v[1]);
			}
			return d;
		}

		public static string ForQuery(this UrlHelper helper, string solrQuery)
		{
			return helper.SetParameter("q", solrQuery);
		}
	}
}
