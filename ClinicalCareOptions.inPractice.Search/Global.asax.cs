﻿using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Configuration;
using SolrNet.Impl;
using SolrNet;
using ClinicalCareOptions.inPractice.Search.App_Start;
using ClinicalCareOptions.inPractice.Search.Models;

namespace ClinicalCareOptions.inPractice.Search
{
    public class Global : HttpApplication
	{
		static readonly string hepatologyUrl = WebConfigurationManager.AppSettings["hepatology"];
        static readonly string rheumatologyUrl = WebConfigurationManager.AppSettings["rheumatology"];
        static readonly string guidelinesUrl = WebConfigurationManager.AppSettings["guidelines"];
        static readonly string drugsUrl = WebConfigurationManager.AppSettings["drugs"];
        static readonly string oncologyUrl = WebConfigurationManager.AppSettings["oncology"];
        static readonly string oncologyNursingUrl = WebConfigurationManager.AppSettings["oncologyNursing"];
        static readonly string hivUrl = WebConfigurationManager.AppSettings["hiv"];


        protected void Application_Start()
		{
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            var hepatologyConnection = new SolrConnection(hepatologyUrl);
            var rheumatologyConnection = new SolrConnection(rheumatologyUrl);
            var guidelinesConnection = new SolrConnection(guidelinesUrl);
            var drugsConnection = new SolrConnection(drugsUrl);
            var oncologyConnection = new SolrConnection(oncologyUrl);
            var oncologyNursingConnection = new SolrConnection(oncologyNursingUrl);
            var hivConnection = new SolrConnection(hivUrl);

            var hepatologyLogging = new LoggingConnection(hepatologyConnection);
            var rheumatologyLogging = new LoggingConnection(rheumatologyConnection);
            var guidelinesLogging = new LoggingConnection(guidelinesConnection);
            var drugsLogging = new LoggingConnection(drugsConnection);
            var oncologyLogging = new LoggingConnection(oncologyConnection);
            var oncologyNursingLogging = new LoggingConnection(oncologyNursingConnection);
            var hivLogging = new LoggingConnection(hivConnection);

            Startup.Init<Hepatology>(hepatologyLogging);
            Startup.Init<Rheumatology>(rheumatologyLogging);
            Startup.Init<Guidelines>(guidelinesLogging);
            Startup.Init<Drugs>(drugsLogging);
            Startup.Init<Oncology>(oncologyLogging);
            Startup.Init<OncologyNursing>(oncologyNursingLogging);
            Startup.Init<HIV>(hivLogging);

            AreaRegistration.RegisterAllAreas();
		}

	}
}
