﻿namespace ClinicalCareOptions.inPractice.Search.Models
{
    public enum Collections
    {
        Hepatology, Rheumatology, Guidelines, Drugs, Oncology, OncologyNursing, HIV
    }
}