﻿namespace ClinicalCareOptions.inPractice.Search.Models
{
    public interface IDocument
    {
        string FormattedTitle();
        string FormattedSummary();
        string FormattedUrl();
    }
}
