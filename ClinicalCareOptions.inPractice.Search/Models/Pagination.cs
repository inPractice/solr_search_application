﻿using System;

namespace ClinicalCareOptions.inPractice.Search
{
    public class Pagination
    {
        int CurrentPage;
        int TotalResults;
        int MaxResults;

        public Pagination(int currentPage, int totalResults, int maxResults)
        {
            CurrentPage = currentPage;
            TotalResults = totalResults;
            MaxResults = maxResults;
        }

        public int LastPage
        {
            get
            {
                return (int)Math.Floor(((decimal)TotalResults - 1) / MaxResults) + 1;
            }
        }

        public bool HasNextPage
        {
            get
            {
                return CurrentPage < LastPage;
            }
        }

        public bool HasPrevPage
        {
            get
            {
                return CurrentPage > 1;
            }
        }

    }
}