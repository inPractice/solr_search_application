<?xml version="1.0" encoding="UTF-8"?>
<!-- 
   Copyright 2010-2015 Norconex Inc.

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
-->
<!-- This configuration shows the minimum required and basic recommendations
     to run a crawler.  
     -->
<httpcollector id="inPractice - Oncology Nursing">

  #set($http = "com.norconex.collector.http")
  #set($core = "com.norconex.collector.core")
  #set($importerHandler = "com.norconex.importer.handler")
  #set($urlNormalizer   = "${http}.url.impl.GenericURLNormalizer")
  #set($filterExtension = "${core}.filter.impl.ExtensionReferenceFilter")
  #set($filterRegexRef  = "${core}.filter.impl.RegexReferenceFilter")
  #set($transformerStripBetween = "${importerHandler}.transformer.impl.StripBetweenTransformer")

  <!-- Decide where to store generated files. -->
  <progressDir>../crawlers/oncologynursing/progress</progressDir>
  <logsDir>../crawlers/oncologynursing/logs</logsDir>

  <crawlers>
    <crawler id="inPractice - Oncology Nursing Crawler">

      <!-- Requires at least one start URL (or urlsFile). 
           Optionally limit crawling to same protocol/domain/port as 
           start URLs. -->
      <startURLs stayOnDomain="true" stayOnPort="true" stayOnProtocol="true">
        <url>http://stagingserver1.inpractice.com/Textbooks/oncology-nursing.aspx</url>
      </startURLs>

      <!-- === Recommendations: ============================================ -->

      <!-- Specify a crawler default directory where to generate files. -->
      <workDir>../crawlers/oncologynursing/workDir</workDir>

      <!-- Put a maximum depth to avoid infinite crawling (e.g. calendars). -->
      <maxDepth>15</maxDepth>
<!--       <maxDocuments>10</maxDocuments> -->

      <!-- We know we don't want to crawl the entire site, so ignore sitemap. -->
      <!-- Before 2.3.0: -->
      <sitemap ignore="true" />
      <!-- Since 2.3.0: -->
      <sitemapResolverFactory ignore="true" />

      <!-- Be as nice as you can to sites you crawl. -->
      <delay default="50" />
      
      <referenceFilters>
      	<filter class="$filterExtension" onMatch="exclude">jpg,jpeg,gif,png,ico,css,js</filter>
	<filter class="$filterRegexRef" onMatch="exclude">.*\?.*</filter>
	<filter class="$filterRegexRef" onMatch="exclude">http://.*\.inpractice\.com/Textbooks/oncology-nursing/Updates\.aspx</filter>
        <filter class="$filterRegexRef" onMatch="exclude">http://.*\.inpractice\.com/Textbooks/.*/Summary\.aspx</filter>
        <filter class="$filterRegexRef" onMatch="exclude">http://.*\.inpractice\.com/Textbooks/.*/References\.aspx</filter>
      	<filter class="$filterRegexRef">http://.*\.inpractice\.com/Textbooks/oncology-nursing.*</filter>
      </referenceFilters>

      <documentFilters>
        <filter class="$filterRegexRef" onMatch="exclude">http://.*\.inpractice\.com/Textbooks/oncology-nursing\.aspx</filter>
        <filter class="$filterRegexRef" onMatch="exclude">http://.*\.inpractice\.com/Textbooks/Oncology-Nursing/Symptom-Management/Metabolic-Effects/Chapter-Pages/Page-1</filter>
      </documentFilters>

      <httpClientFactory class="com.norconex.collector.http.client.impl.GenericHttpClientFactory">
        <maxConnections>100</maxConnections>
        <connectionTimeout>30000</connectionTimeout>
      </httpClientFactory>

      <importer>
        <preParseHandlers>
            <transformer class="$transformerStripBetween" inclusive="true" >
	      <restrictTo field="document.contentType">text/html</restrictTo>
              <stripBetween>
                <start>&lt;!--googleoff:index--&gt;</start>
                <end>&lt;!--googleon:index--&gt;</end>
              </stripBetween>
              <stripBetween>
                <start>&lt;!--googleoff: index--&gt;</start>
                <end>&lt;!--googleon: index--&gt;</end>
              </stripBetween>
              <stripBetween>
              	<start>&lt;!--googleoff:all--&gt;</start>
                <end>&lt;!--googleon:all--&gt;</end>
              </stripBetween>
              <stripBetween>
                <start>&lt;!--googleoff: all--&gt;</start>
                <end>&lt;!--googleon: all--&gt;</end>
              </stripBetween>
  	    </transformer>
	</preParseHandlers>
        <postParseHandlers>
            <tagger class="com.norconex.importer.handler.tagger.impl.ConstantTagger">
              <constant name="section">Oncology Nursing</constant>
            </tagger>
        </postParseHandlers>
      </importer> 
      <!-- Document importing -->
<!-- 
      <importer>
        <postParseHandlers>
          <!~~ If your target repository does not support arbitrary fields,
               make sure you only keep the fields you need. ~~>
          <tagger class="com.norconex.importer.handler.tagger.impl.KeepOnlyTagger">
            <fields>title,keywords,description,document.reference</fields>
          </tagger>
        </postParseHandlers>
      </importer> 
 -->
      

		<committer class="com.norconex.committer.solr.SolrCommitter">
  	  		<solrURL>http://localhost:8983/solr/oncology-nursing</solrURL>
  	  		<commitDisabled>false</commitDisabled>
  	  		<queueDir>../crawlers/oncologynursing/committer-queue</queueDir>
  	  		<targetContentField>body</targetContentField>
  	  		<queueSize>100</queueSize>
			<commitBatchSize>500</commitBatchSize>
 		</committer>
 	</crawler>

  </crawlers>
  
</httpcollector>
