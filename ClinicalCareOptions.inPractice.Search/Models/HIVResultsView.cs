﻿using ClinicalCareOptions.inPractice.Search.Models;
using System.Collections.Generic;

namespace ClinicalCareOptions.inPractice.Search
{
	public class HIVResultsView
    {
		public HIVResultsView()
		{
			Params = new Parameters();
			Documents = new List<HIV>();
        }

        public Parameters Params { get; set; }
		public ICollection<HIV> Documents { get; set; }
		public int Count { get; set; }
		public string DidYouMean { get; set; }
		public bool Error { get; set; }
        public Pagination Pagination
        {
            get
            {
                return new Pagination(Params.PageNum, Count, Params.MaxResultsPage);
            }
        }

    }
}