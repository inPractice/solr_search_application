﻿using System.Collections.Generic;
using SolrNet.Attributes;

namespace ClinicalCareOptions.inPractice.Search.Models
{
	public class Guidelines : IDocument
	{
		[SolrUniqueKey("id")]
		public string Id { get; set; }

		[SolrUniqueKey("pageType")]
		public string PageType { get; set; }

		[SolrUniqueKey("title")]
		public ICollection<string> Titles { get; set; }

		[SolrUniqueKey("date")]
		public string Date { get; set; }

		[SolrUniqueKey("guid")]
		public string Guid { get; set; }

		[SolrUniqueKey("keywords")]
		public string Keywords { get; set; }

		[SolrUniqueKey("searchSource")]
		public string SearchSource { get; set; }

		[SolrUniqueKey("searchDescription")]
		public string SearchDescription { get; set; }

		[SolrUniqueKey("searchTitle")]
		public string SearchTitle { get; set; }

        [SolrUniqueKey("searchLink")]
        public string SearchLink { get; set; }

        [SolrUniqueKey("searchUrl")]
        public string SearchUrl { get; set; }

        [SolrUniqueKey("subsite")]
		public string Subsite { get; set; }

		[SolrUniqueKey("referringPage")]
		public string ReferringPage { get; set; }

		[SolrUniqueKey("isPoc")]
		public bool IsPoc { get; set; }

		[SolrUniqueKey("recentView")]
		public string RecentView { get; set; }

		[SolrUniqueKey("pageviewId")]
		public string PageviewId { get; set; }

		[SolrUniqueKey("serialNumber")]
		public string SerialNumber { get; set; }

        public string FormattedSummary()
        {
            return SearchDescription;
        }

        public string FormattedTitle()
        {
            if (string.IsNullOrEmpty(SearchTitle))
            {
                return "Untitled";
            }
            return SearchTitle;
        }

        public string FormattedUrl()
        {
            if (string.IsNullOrEmpty(SearchUrl))
            {
                return Id;
            }
            return SearchUrl;
        }
    }
}
